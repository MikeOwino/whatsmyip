const express = require('express');
const requestIp = require('request-ip');

const app = express();

app.use(requestIp.mw());

app.get('/', (req, res) => {
  const clientIp = req.clientIp;
  res.send(`${clientIp}`);
});

app.listen(3000, () => {
  console.log('Server listening on port 3000');
});
